export const environment = {
  production: true,
  backendUrl: 'https://api.tourdeplanet.org',
  locationApiUrl: 'https://nominatim.openstreetmap.org/'
};
