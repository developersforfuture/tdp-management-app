export const environment = {
  production: true,
  backendUrl: 'https://api.staging.tourdeplanet.org',
  locationApiUrl: 'https://nominatim.openstreetmap.org/'
};
