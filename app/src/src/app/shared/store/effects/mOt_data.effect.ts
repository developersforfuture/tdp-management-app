import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import * as motDataActions from './../actions/mOt_data.action';
import { DataService } from 'src/app/shared/services';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';

@Injectable()
export class MeansOfTransportationDataEffect {
    constructor(
        private actions$: Actions,
        private dataService: DataService
    ) {}

    @Effect()
    loadMeansOfTransportation$: Observable<Action> = this.actions$.pipe(
        ofType(motDataActions.LOAD_MEANSOFTRANSPORTATION),
        mergeMap(() => this.dataService.getMeanOfTransportation()),
        map((m: MeanOfTransportationModel[] ) => new motDataActions.LoadMeansOfTransportationSuccessAction(m)),
        catchError(error => of(new motDataActions.LoadMeansOfTransportationFailAction(error)))
    );
}
