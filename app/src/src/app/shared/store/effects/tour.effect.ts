import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import * as tourActions from '../actions/tours.action';
import { TourModel } from 'src/app/shared/models/tour.model';
import { TourService } from '../../services/tour.service';

@Injectable()
export class ToursEffect {
    constructor(
        private actions$: Actions,
        private tourService: TourService
    ) {}

    @Effect()
    loadTours$: Observable<Action> = this.actions$.pipe(
        ofType(tourActions.LOAD_TOURS),
        mergeMap(() => this.tourService.getList()),
        map((tours: TourModel[]) => new tourActions.LoadToursSuccess(tours)),
        catchError(error => of(new tourActions.LoadToursFail(error)))
    );

    @Effect()
    createTour$: Observable<Action> = this.actions$.pipe(
        ofType(tourActions.CREATE_NEW_TOUR),
        map((action: tourActions.CreateTour) => action.payload),
        mergeMap((tour: TourModel) => this.tourService.create(tour)),
        map((mappedTour: TourModel) => new tourActions.CreateTourSuccess(mappedTour)),
        catchError(error => of(new tourActions.CreateTourFail(error)))
    );

    @Effect()
    updateTour$: Observable<Action> = this.actions$.pipe(
        ofType(tourActions.UPDATE_TOUR),
        map((action: tourActions.UpdateTour) => action.payload),
        mergeMap((tour: TourModel) => this.tourService.update(tour)),
        map(mappedTour => new tourActions.UpdateTourSuccess(mappedTour)),
        catchError(error => of(new tourActions.UpdateTourFail(error)))
    );


    @Effect()
    removeTour$: Observable<Action> = this.actions$.pipe(
        ofType(tourActions.REMOVE_TOUR),
        map((action: tourActions.RemoveTour) => action.payload),
        mergeMap((tour: TourModel) => this.tourService.remove(tour)),
        map((tour) => new tourActions.RemoveTourSuccess(tour.id)),
        catchError(error => of(new tourActions.UpdateTourFail(error)))
    );
}
