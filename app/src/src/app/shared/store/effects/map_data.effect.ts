import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import * as mapDataActions from '../actions/map_data.action';
import { MapModel } from 'src/app/shared/models/map.model';
import { MapDataService } from 'src/app/modules/map/services/map-data.service';

@Injectable()
export class MapDataEffect {
    constructor(
        private actions$: Actions,
        private mapDataService: MapDataService
    ) {}

    @Effect()
    loadMap$: Observable<Action> = this.actions$.pipe(
        ofType(mapDataActions.LOAD_MAP),
        mergeMap(() => this.mapDataService.getMapData()),
        map((m: MapModel) => new mapDataActions.LoadMapSuccessAction(m)),
        catchError(error => of(new mapDataActions.LoadMapFailAction(error)))
    );
}
