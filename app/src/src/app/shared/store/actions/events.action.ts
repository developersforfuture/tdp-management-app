import {Action} from '@ngrx/store';
import { TourEvent } from 'src/app/shared/models/tour_event.model';

export const LOAD_EVENTS = '[Events] Load events';
export const LOAD_EVENTS_FAIL = '[Events] Load events failed';
export const LOAD_EVENTS_SUCCESS = '[Events] Load events succeeded';

export const CREATE_NEW_EVENT = '[Events] Create new event.';
export const CREATE_NEW_EVENT_FAIL = '[Events] Create new event failed.';
export const CREATE_NEW_EVENT_SUCCESS = '[Events] Create new event succeeded.';

export const UPDATE_EVENT = '[Events] Update event.';
export const UPDATE_EVENT_FAIL = '[Events] Update event failed.';
export const UPDATE_EVENT_SUCCESS = '[Events] Update event succeeded.';

export const REMOVE_EVENT = '[Events] Remove event.';
export const REMOVE_EVENT_FAIL = '[Events] Remove event failed.';
export const REMOVE_EVENT_SUCCESS = '[Events] Remove event succeeded.';

export class LoadEvents implements Action {
    readonly type = LOAD_EVENTS;
}

export class LoadEventsFail implements Action {
    readonly type = LOAD_EVENTS_FAIL;
    constructor(public payload: any[]) {}
}

export class LoadEventsSuccess implements Action {
    readonly type = LOAD_EVENTS_SUCCESS;
    constructor(public payload: TourEvent[]) {}
}

export class CreateEvent implements Action {
    readonly type = CREATE_NEW_EVENT;
    constructor(public payload: TourEvent) {}
}

export class CreateEventFail implements Action {
    readonly type = CREATE_NEW_EVENT_FAIL;
    constructor(public payload: any[]) {}
}

export class CreateEventSuccess implements Action {
    readonly type = CREATE_NEW_EVENT_SUCCESS;
    constructor(public payload: TourEvent) {}
}

export class UpdateEvent implements Action {
    readonly type = UPDATE_EVENT;
    constructor(public payload: TourEvent) {}
}

export class UpdateEventFail implements Action {
    readonly type = UPDATE_EVENT_FAIL;
    constructor(public payload: any[]) {}
}

export class UpdateEventSuccess implements Action {
    readonly type = UPDATE_EVENT_SUCCESS;
    constructor(public payload: TourEvent) {}
}

export class RemoveEvent implements Action {
    readonly type = REMOVE_EVENT;
    constructor(public payload: TourEvent) {}
}

export class RemoveEventFail implements Action {
    readonly type = REMOVE_EVENT_FAIL;
    constructor(public payload: any[]) {}
}

export class RemoveEventSuccess implements Action {
    readonly type = REMOVE_EVENT_SUCCESS;
    constructor(public payload: number) {}
}

// action types
export type EventsAction =
    | LoadEvents
    | LoadEventsFail
    | LoadEventsSuccess
    | CreateEvent
    | CreateEventFail
    | CreateEventSuccess
    | UpdateEvent
    | UpdateEventFail
    | UpdateEventSuccess
    | RemoveEvent
    | RemoveEventFail
    | RemoveEventSuccess;
