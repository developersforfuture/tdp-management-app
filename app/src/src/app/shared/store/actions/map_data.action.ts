import {Action} from '@ngrx/store';
import { MapModel } from 'src/app/shared/models/map.model';
import { TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';
import { TourModel } from '../../models/tour.model';

export const LOAD_MAP = '[Map] Load map data';
export const LOAD_MAP_FAIL = '[Map] Load map data failed';
export const LOAD_MAP_SUCCESS = '[Map] Load map data succeeded';

export const UPDATE_ATTENDEES_DONATION = '[Map] updatate attendees donations.';

export const ADD_TOUR_IN_WORKING_MODE = '[Map] add tour in working mode';
export const UPDATE_TOUR_IN_WORKING_MODE = '[Map] update tour in working mode';
export const REMOVE_TOUR_FROM_WORKING_MODE = '[Map] remove tour from working mode';

export class LoadMapAction implements Action {
    readonly type = LOAD_MAP;
}

export class LoadMapFailAction implements Action {
    readonly type = LOAD_MAP_FAIL;
    constructor(public payload: any[]) {}
}

export class LoadMapSuccessAction implements Action {
    readonly type = LOAD_MAP_SUCCESS;
    constructor(public payload: MapModel) {}
}

export class UpdateAttendeesDonationAction implements Action {
    readonly type = UPDATE_ATTENDEES_DONATION;
    constructor(public payload: TourAttendeeDonation[]) {}
}

export class AddTourInWorkingMode implements Action {
  readonly type = ADD_TOUR_IN_WORKING_MODE;
  constructor(public payload: TourModel) {}
}

export class RemovTourFromWorkingMode implements Action {
  readonly type = REMOVE_TOUR_FROM_WORKING_MODE;
  constructor(public payload: TourModel) {}
}

export class UpdateTourInWorkingMode implements Action {
  readonly type = UPDATE_TOUR_IN_WORKING_MODE;
  constructor(public payload: TourModel) {}
}
// action types
export type MapAction =
    | LoadMapAction
    | LoadMapFailAction
    | LoadMapSuccessAction
    | UpdateAttendeesDonationAction
    | UpdateTourInWorkingMode
    | AddTourInWorkingMode
    | UpdateTourInWorkingMode
    | RemovTourFromWorkingMode;
