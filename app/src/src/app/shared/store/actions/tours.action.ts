import {Action} from '@ngrx/store';
import { TourModel } from 'src/app/shared/models/tour.model';

export const LOAD_TOURS = '[Tours] Load tours';
export const LOAD_TOURS_FAIL = '[Tours] Load tours failed';
export const LOAD_TOURS_SUCCESS = '[Tours] Load tours succeeded';

export const CREATE_NEW_TOUR = '[Tours] Create new tour.';
export const CREATE_NEW_TOUR_FAIL = '[Tours] Create new tour failed.';
export const CREATE_NEW_TOUR_SUCCESS = '[Tours] Create new tour succeeded.';

export const UPDATE_TOUR = '[Tours] Update tour.';
export const UPDATE_TOUR_FAIL = '[Tours] Update tour failed.';
export const UPDATE_TOUR_SUCCESS = '[Tours] Update tour succeeded.';

export const REMOVE_TOUR = '[Tours] Remove tour.';
export const REMOVE_TOUR_FAIL = '[Tours] Remove tour failed.';
export const REMOVE_TOUR_SUCCESS = '[Tours] Remove tour succeeded.';

export class LoadTours implements Action {
    readonly type = LOAD_TOURS;
}

export class LoadToursFail implements Action {
    readonly type = LOAD_TOURS_FAIL;
    constructor(public payload: any[]) {}
}

export class LoadToursSuccess implements Action {
    readonly type = LOAD_TOURS_SUCCESS;
    constructor(public payload: TourModel[]) {}
}

export class CreateTour implements Action {
    readonly type = CREATE_NEW_TOUR;
    constructor(public payload: TourModel) {}
}

export class CreateTourFail implements Action {
    readonly type = CREATE_NEW_TOUR_FAIL;
    constructor(public payload: any[]) {}
}

export class CreateTourSuccess implements Action {
    readonly type = CREATE_NEW_TOUR_SUCCESS;
    constructor(public payload: TourModel) {}
}

export class UpdateTour implements Action {
    readonly type = UPDATE_TOUR;
    constructor(public payload: TourModel) {}
}

export class UpdateTourFail implements Action {
    readonly type = UPDATE_TOUR_FAIL;
    constructor(public payload: any[]) {}
}

export class UpdateTourSuccess implements Action {
    readonly type = UPDATE_TOUR_SUCCESS;
    constructor(public payload: TourModel) {}
}

export class RemoveTour implements Action {
    readonly type = REMOVE_TOUR;
    constructor(public payload: TourModel) {}
}

export class RemoveTourFail implements Action {
    readonly type = REMOVE_TOUR_FAIL;
    constructor(public payload: any[]) {}
}

export class RemoveTourSuccess implements Action {
    readonly type = REMOVE_TOUR_SUCCESS;
    constructor(public payload: number) {}
}

// action types
export type ToursAction =
    | LoadTours
    | LoadToursFail
    | LoadToursSuccess
    | CreateTour
    | CreateTourFail
    | CreateTourSuccess
    | UpdateTour
    | UpdateTourFail
    | UpdateTourSuccess
    | RemoveTour
    | RemoveTourFail
    | RemoveTourSuccess;
