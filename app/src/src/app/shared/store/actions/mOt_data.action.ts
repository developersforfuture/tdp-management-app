import {Action} from '@ngrx/store';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';

export const LOAD_MEANSOFTRANSPORTATION = '[MeansOfTransportation] Load meansoftransportation data';
export const LOAD_MEANSOFTRANSPORTATION_FAIL = '[MeansOfTransportation] Load meansoftransportation data failed';
export const LOAD_MEANSOFTRANSPORTATION_SUCCESS = '[MeansOfTransportation] Load meansoftransportation data succeeded';

export class LoadMeansOfTransportationAction implements Action {
    readonly type = LOAD_MEANSOFTRANSPORTATION;
}

export class LoadMeansOfTransportationFailAction implements Action {
    readonly type = LOAD_MEANSOFTRANSPORTATION_FAIL;
    constructor(public payload: any[]) {}
}

export class LoadMeansOfTransportationSuccessAction implements Action {
    readonly type = LOAD_MEANSOFTRANSPORTATION_SUCCESS;
    constructor(public payload: MeanOfTransportationModel[]) {}
}


// action types
export type MeansOfTransportationAction =
    | LoadMeansOfTransportationAction
    | LoadMeansOfTransportationFailAction
    | LoadMeansOfTransportationSuccessAction;
