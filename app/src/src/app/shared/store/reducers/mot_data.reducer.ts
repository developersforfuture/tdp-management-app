import * as fromMeansOfTransportationActions from '../actions/mOt_data.action';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';

export interface MeansOfTransportationState {
    entities: {[id: string]: MeanOfTransportationModel};
    loaded: boolean;
    loading: boolean;
}

export const initialState: MeansOfTransportationState = {
    entities: {},
    loaded: false,
    loading: false,
};

export function reducer(
    state = initialState,
    action: fromMeansOfTransportationActions.MeansOfTransportationAction
): MeansOfTransportationState {
    switch (action.type) {
        case fromMeansOfTransportationActions.LOAD_MEANSOFTRANSPORTATION: {
            return {
                ...state,
                loading: true,
            };
        }

        case fromMeansOfTransportationActions.LOAD_MEANSOFTRANSPORTATION_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false,
            };
        }

        case fromMeansOfTransportationActions.LOAD_MEANSOFTRANSPORTATION_SUCCESS: {
            const mOtData = action.payload;

            const entities = mOtData.reduce(
                (e: { [id: string]: MeanOfTransportationModel }, entity: MeanOfTransportationModel) => {
                    return { ...e, [entity.id]: entity};
                }, {});

            return {
                ...state,
                loading: false,
                loaded: true,
                entities
            };
        }
    }

    return state;
}

export const getMeansOfTransportationEntities = (state: MeansOfTransportationState) => state.entities;
export const getLoading = (state: MeansOfTransportationState) => state.loading;
export const getLoaded = (state: MeansOfTransportationState) => state.loaded;
