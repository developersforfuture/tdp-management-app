import * as fromMap from '../actions/map_data.action';
import { TourModel as Tour } from 'src/app/shared/models/tour.model';
import { TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';
import { TourEvent } from 'src/app/shared/models/tour_event.model';
export interface MapDataState {
    donations: {[id: string]: TourAttendeeDonation};
    tours: {[id: number]: Tour};
    events: {[id: number]: TourEvent};
    workingTours: {[id: number]: Tour};
    loaded: boolean;
    loading: boolean;
}

export const initialState: MapDataState = {
    donations: {},
    tours: {},
    events: {},
    workingTours: {},
    loaded: false,
    loading: false,
};

export function reducer(
    state = initialState,
    action: fromMap.MapAction
): MapDataState {
    switch (action.type) {
        case fromMap.LOAD_MAP: {
            return {
                ...state,
                loading: true,
            };
        }

        case fromMap.LOAD_MAP_FAIL: {
            return {
                ...state,
                loading: false,
                loaded: false,
            };
        }

        case fromMap.LOAD_MAP_SUCCESS: {
            const mapData = action.payload;

            const tours = mapData.tours.reduce(
                (e: { [id: number]: Tour }, tour: Tour) => {
                    return { ...e, [tour.id]: tour};
                },
                { ...state.tours});

            const events = mapData.events.reduce(
                (e: { [id: number]: TourEvent }, event: TourEvent) => {
                    return { ...e, [event.id]: event};
                },
                { ...state.events});

            const donations = mapData.donations.reduce(
                (e: { [id: string]: TourAttendeeDonation }, d: TourAttendeeDonation) => {
                    return { ...e, [d.id]: d};
                },
                { ...state.donations});

            return {
                ...state,
                loading: false,
                loaded: true,
                tours,
                events,
                donations
            };
        }

        case fromMap.UPDATE_ATTENDEES_DONATION: {
            const donationsInPayload = action.payload;
            const donations = donationsInPayload.reduce((
                e: {[id: string]: TourAttendeeDonation},
                d: TourAttendeeDonation
              ): {[id: string]: TourAttendeeDonation} => {
                   return {...e, [d.id]: d};
                }, {...state.donations});

            return {
                ...state,
                loading: false,
                loaded: true,
                donations
            };
        }
        case fromMap.ADD_TOUR_IN_WORKING_MODE: {
          const tourInPayload = action.payload;
          return {...state, [tourInPayload.id]: tourInPayload};
        }

        case fromMap.UPDATE_TOUR_IN_WORKING_MODE: {
          const tourInWorkingState = state.hasOwnProperty(action.payload.id) ? state[action.payload.id] : null;
          if (null === tourInWorkingState) {
            return state;
          }
          return {...state, [action.payload.id]: action.payload};
        }


        case fromMap.REMOVE_TOUR_FROM_WORKING_MODE: {
          const tourInWorkingState = state.hasOwnProperty(action.payload.id) ? state[action.payload.id] : null;
          if (null === tourInWorkingState) {
            return state;
          }
          delete state[action.payload.id];
          return {...state};
        }
    }

    return state;
}

export const getMapTourEntities = (state: MapDataState) => state.tours;
export const getMapEventEntities = (state: MapDataState) => state.events;
export const getMapDonationEntities = (state: MapDataState) => state.donations;
export const getLoading = (state: MapDataState) => state.loading;
export const getLoaded = (state: MapDataState) => state.loaded;
