import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromMap from '../reducers/map_data.reducer';
import { TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';

export const getMapState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.map
);

export const getMapDonationEntities = createSelector(
    getMapState,
    fromMap.getMapDonationEntities
);

export const getAllDonationsOnMap = createSelector(getMapDonationEntities, entities => {
    return Object.keys(entities).map(id => entities[id]);
});

export const getMapDonationEntitiesAsGeoJSON = createSelector(
  getAllDonationsOnMap,
  (donations) => {
    const features = donations.map((donation: TourAttendeeDonation) => {
      return {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [
            donation.point.longitude,
            donation.point.latitude
          ]
        },
        properties: {
          id: 'donation_' + donation.id,
          amount: donation.amount,
          by: donation.by,
          date: donation.date,
          point: donation.point,
          unit: donation.unit,
          type: 'Donation',
        }
      };
    });

    return {type: 'FeatureCollection', features};
  }
);

export const getMapTourEntities = createSelector(
    getMapState,
    fromMap.getMapTourEntities
);

export const getAllToursOnMap = createSelector(getMapTourEntities, entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});


export const getMapEventEntities = createSelector(
    getMapState,
    fromMap.getMapEventEntities
);

export const getAllEventsOnMap = createSelector(getMapEventEntities, entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});

export const getMapDataLoaded = createSelector(
    getMapState,
    fromMap.getLoaded
);

export const getMapDataLoading = createSelector(
    getMapState,
    fromMap.getLoading
);

export const getGetToursInWorkingState = createSelector(
  getMapState,
  (state) => state.workingTours
);
