import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromTours from '../reducers/tour.reducer';

export const getToursState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.tours
);

export const getTourEntities = createSelector(
    getToursState,
    fromTours.getTourEntities
);

export const getAllTours = createSelector(getTourEntities, entities => {
    return Object.keys(entities).map(id => entities[parseInt(id, 10)]);
});

export const getToursLoaded = createSelector(
    getToursState,
    fromTours.getLoaded
);

export const getToursLoading = createSelector(
    getToursState,
    fromTours.getLoading
);
