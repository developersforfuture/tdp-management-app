import {createSelector} from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromMOT from '../reducers/mot_data.reducer';

export const getMeansOfTransportationState = createSelector(
    fromFeature.getAppState,
    (state: fromFeature.AppState) => state.mot
);

export const getMeansOfTransportationAsEntities = createSelector(
    getMeansOfTransportationState,
    fromMOT.getMeansOfTransportationEntities
);

export const getMeansOfTransportation = createSelector(getMeansOfTransportationAsEntities, entities => {
    return Object.keys(entities).map(id => entities[id]);
});
