import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class DonationsService {

  constructor(private http: HttpClient, private auth: AuthenticationService) { }

  getAttendeeDonations(userId?: string): Observable<TourAttendeeDonation[]> {
    userId = userId || this.getCurrentUserId();
    return this.http
      .get<TourAttendeeDonation[]>(`${environment.backendUrl}/users/${userId}/attendee-donations`)
      .pipe(catchError((error: any) => {
        return Observable.throw(error);
      }));
  }

  addAttendeeDonation(donation: TourAttendeeDonation, userId?: string): Observable<TourAttendeeDonation[]> {
    userId = userId || this.getCurrentUserId();
    return this.http
      .post<TourAttendeeDonation[]>(`${environment.backendUrl}/users/${userId}/attendee-donations`, donation)
      .pipe(catchError((error: any) => {
        return Observable.throw(error);
      }));
  }

  private getCurrentUserId(): string {
    const userToken = this.auth.currentUserTokenValue;
    if (null == userToken) {
      return '';
    }

    return userToken.userValues.userId;
  }
}
