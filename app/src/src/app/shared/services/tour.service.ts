import { Injectable } from '@angular/core';
import { TourModel } from '../models/tour.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TourService {

  constructor(private http: HttpClient) { }

  create(tour: TourModel): Observable<{ Tour } | any> {
    return this.http
      .post<TourModel>(environment.backendUrl + `/tour`, tour)
      .pipe(catchError((error: any) => {
        return Observable.throw(error);
      }));
  }

  getList(): Observable<TourModel[]> {
    return this.http.
      get<TourModel[]>(`${environment.backendUrl}/tours`)
      .pipe(catchError((error: any) => {
        return Observable.throw(error);
      }));
  }

  get(id: number): Observable<TourModel> {
    return this.http.get<TourModel>(`${environment.backendUrl}/tours/${id}`).pipe(catchError((error: any) => {
      return Observable.throw(error);
    }));
  }

  update(tour: TourModel): Observable<TourModel> {
    const id = tour.id;
    return this.http.put<TourModel>(`${environment.backendUrl}/tours/${id}`, tour).pipe(catchError((error: any) => {
      return Observable.throw(error);
    }));
  }

  remove(tour: TourModel): Observable<TourModel> {
    const id = tour.id;
    return this.http.delete<TourModel>(`${environment.backendUrl}/tours/${id}`).pipe(catchError((error: any) => {
      return Observable.throw(error);
    }));
  }
}
