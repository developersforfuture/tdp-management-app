import { FormControl } from '@angular/forms';

export class MeanOfTransportationModel {
    id: string;
    name: string;
    iconClass: string;
    color: string;
}

export class MeanOfTransportationModelFormModel {
  id = new FormControl();

  constructor(motId: string) {
      this.id.setValue(motId);
  }
}
