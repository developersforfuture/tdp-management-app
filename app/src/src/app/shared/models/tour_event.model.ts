import { TourPoint, TourPointForm } from './tour_point.model';
import { FormControl, FormBuilder } from '@angular/forms';

export interface TourEvent {
    id: number;
    point: TourPoint;
    author: string;
    title: string;
    url: string;
    date_to: number;
    date_from: number;
}

export class TourEventFormModel {
    id = new FormControl();
    point = new FormControl();
    author = new FormControl();
    title = new FormControl();
    url = new FormControl();
    date_to = new FormControl();
    date_from = new FormControl();

    constructor(event: TourEvent, fb: FormBuilder) {
        if (event.id) {
            this.id.setValue(event.id);
        }
        this.point.setValue(fb.group(new TourPointForm(event.point)));
        this.author.setValue(event.author);
        this.title.setValue(event.title);
        this.url.setValue(event.url);
        this.date_from.setValue(event.date_from);
        this.date_to.setValue(event.date_to);
    }
}
