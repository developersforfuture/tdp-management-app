import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromDataStore from 'src/app/shared/store';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-means-of-transportation',
  templateUrl: './means-of-transportation.component.html',
  styleUrls: ['./means-of-transportation.component.scss']
})
export class MeansOfTransportationComponent implements OnInit {
  @Input() motForm: FormGroup;
  mots$: Observable <{[id: string]: MeanOfTransportationModel}>;
  constructor(private store: Store<fromDataStore.AppState>) { }

  ngOnInit(): void {
    this.mots$ = this.store.select(fromDataStore.getMeansOfTransportationAsEntities);
  }

  getMots(mots: {[id: string]: MeanOfTransportationModel}): MeanOfTransportationModel[] {
    return Object.keys(mots).map((key) => mots[key]);
  }

}
