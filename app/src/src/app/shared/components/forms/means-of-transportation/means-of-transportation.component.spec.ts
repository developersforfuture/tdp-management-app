import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeansOfTransportationComponent } from './means-of-transportation.component';

describe('MeansOfTransportationComponent', () => {
  let component: MeansOfTransportationComponent;
  let fixture: ComponentFixture<MeansOfTransportationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeansOfTransportationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeansOfTransportationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
