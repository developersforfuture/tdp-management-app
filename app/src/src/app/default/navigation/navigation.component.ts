import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: 'navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  @Output() toggleMenu: EventEmitter<boolean> = new EventEmitter<boolean>();

  onToogleMenu(): void {
    this.toggleMenu.emit(true);
  }
}
