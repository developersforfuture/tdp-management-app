import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserToken } from '../../shared/models/user.model';

@Component({
  selector: 'app-dashboard-overview',
  templateUrl: './dashboard-overview.component.html',
  styleUrls: ['./dashboard-overview.component.scss']
})
export class DashboardOverviewComponent implements OnInit {
  @Input() user: UserToken;
  @Output() toggleMenu: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit(): void {
  }
  get isUser(): boolean {
    return !!(this.user);
  }

  onToogleMenu(): void {
    this.toggleMenu.emit(true);
  }
}
