import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { DefaultComponent } from './default/default.component';
import { MapModule } from 'src/app/modules/map/map.module';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from '../modules/home/components/home/home.component';
import { PageNotFoundComponent } from 'src/app/default/page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AuthGuard } from 'src/app/shared/helpers';
import { NotificationModule } from '../modules/notification-module/notification.module';
import { DonationsModule } from '../modules/donations/donations.module';
import { HeaderComponent } from './header/header.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardOverviewComponent } from './dashboard-overview/dashboard-overview.component';
import { HomeModule } from '../modules/home/home.module';
import { MenueComponent } from './menue/menue.component';
import { AccountMenueComponent } from './accountmenu/accountmenue.component';

export const DEFAULT_ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'events',
    loadChildren: async () =>
      (await import('./../modules/event/event.module')).EventModule,
    canActivate: [AuthGuard],
  },
  {
    path: 'tours',
    loadChildren: async () =>
      (await import('./../modules/tour/tour.module')).TourModule,
    canActivate: [AuthGuard],
  },
  {
    path: 'auth',
    loadChildren: async () =>
      (await import('./../modules/auth/auth.module')).AuthModule,
  },
  {
    path: 'account',
    loadChildren: async () =>
      (await import('./../modules/account/account.module')).AccountModule,
    canActivate: [AuthGuard],
  },
  {
    path: 'donations',
    loadChildren: async () =>
      (await import('./../modules/donations/donations.module')).DonationsModule,
    canActivate: [AuthGuard],
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  declarations: [
    DefaultComponent,
    FooterComponent,
    HeaderComponent,
    NavigationComponent,
    PageNotFoundComponent,
    DashboardOverviewComponent,
    MenueComponent,
    AccountMenueComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(DEFAULT_ROUTES, {useHash: true}),
    MapModule,
    ReactiveFormsModule,
    NotificationModule,
    SharedModule,
    DonationsModule,
    HomeModule,
  ],
  exports: [DefaultComponent],
})
export class DefaultModule {}
