import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { User } from 'src/app/modules/account/models/user.model';

@Component({
  selector: 'app-accountmenue',
  templateUrl: './accountmenue.component.html'
})
export class AccountMenueComponent implements OnInit {
  @Input() user: User;
  @Output() toggleMenu: EventEmitter<boolean> = new EventEmitter();
  constructor() {}

  get isUser(): boolean {
    return !!(this.user);
  }

  ngOnInit(): void {

  }

  onToggleMenu(): void {
    this.toggleMenu.emit(true);
  }
}
