import {
  Component,
  OnInit,
  ChangeDetectorRef,
} from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromAttendeeStore from 'src/app/shared/store';
import * as fromDonationStore from 'src/app/shared/store';
import * as moment from 'moment';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';

@Component({
  selector: 'app-recently-added-donations',
  templateUrl: './recently-added-donations.component.html',
  styleUrls: ['./recently-added-donations.component.scss'],
})
export class RecentlyAddedDonationsComponent implements OnInit {
  latestDonation: TourAttendeeDonation;
  latestDonation$: Observable<TourAttendeeDonation>;
  mot$: Observable<{ [id: string]: MeanOfTransportationModel }>;
  constructor(private store: Store<fromAttendeeStore.AppState>) {}

  ngOnInit(): void {
    this.mot$ = this.store.select(fromAttendeeStore.getMeansOfTransportationAsEntities);
    this.latestDonation$ = this.store.select(fromDonationStore.getAllAttendeeDonations).pipe(
      map((donations) => {
        if (donations.length === 0) {
          return null;
        }
        return donations.sort((a, b) => {
          return moment(a.createdAt, 'dd/mm/yy').isBefore(
            moment(b.createdAt, 'dd/mm/yy')
          )
            ? 1
            : -1;
        })[donations.length - 1];
      })
    );
  }
}
