import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { User } from 'src/app/modules/account/models/user.model';
import { Router } from '@angular/router';
import { UserToken } from 'src/app/shared/models/user.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  userToken$: Observable<UserToken>;
  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    this.userToken$ = this.authService.currentUserToken;
  }

}
