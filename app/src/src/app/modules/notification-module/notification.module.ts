import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification/notification.component';
import { MatCardModule, MatButtonModule, MatIconModule } from '@angular/material';



@NgModule({
  declarations: [NotificationComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [NotificationComponent]
})
export class NotificationModule { }
