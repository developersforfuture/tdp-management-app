import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { EventComponent } from './event/event.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule, MetaReducer } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

export const EVENT_ROUTES: Routes = [
  {path: 'dashboard', component: EventComponent}
];

@NgModule({
  declarations: [EventComponent],
  imports: [
    CommonModule,
    SharedModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forChild(EVENT_ROUTES),
  ],
  exports: [EventComponent],
})
export class EventModule { }
