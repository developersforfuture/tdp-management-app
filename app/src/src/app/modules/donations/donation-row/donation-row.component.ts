import { Component, OnInit, Input } from '@angular/core';
import { TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import { DonationHelperService } from 'src/app/shared/services/donation-helper.service';

@Component({
  selector: 'app-donation-row',
  templateUrl: './donation-row.component.html'
})
export class DonationRowComponent implements OnInit {
  @Input() donation: TourAttendeeDonation = null;
  @Input() mots: {[id: string]: MeanOfTransportationModel} = {};
  constructor() { }

  ngOnInit(): void {
  }

  get iconClass(): string {
    const by = this.donation.by as string;
    return 'icon-' + DonationHelperService.getIconClass(by, this.mots);
  }
  get color(): string {
    const by = this.donation.by as string;
    return DonationHelperService.getColor(by, this.mots);
  }
}
