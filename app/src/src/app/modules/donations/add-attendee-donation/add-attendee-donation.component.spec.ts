import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAttendeeDonationComponent } from './add-attendee-donation.component';

describe('AddAttendeeDonationComponent', () => {
  let component: AddAttendeeDonationComponent;
  let fixture: ComponentFixture<AddAttendeeDonationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAttendeeDonationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAttendeeDonationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
