import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import * as fromDonationStore from 'src/app/shared/store';
import * as fromAttendeeStore from 'src/app/shared/store';
import { Store } from '@ngrx/store';
import { TourAttendeeDonation } from 'src/app/shared/models/tour_attendee_donation.model';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-attendee-donation',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './attendee-donation.component.html',
  styleUrls: ['./attendee-donation.component.scss']
})
export class AttendeeDonationComponent implements OnInit, OnDestroy {
  meansOfTransportation: {[id: string]: MeanOfTransportationModel} = {};
  donations: TourAttendeeDonation[] = [];
  dSubscribtion: Subscription;
  constructor(private store: Store<fromDonationStore.AppState>, private ref: ChangeDetectorRef) {}

  ngOnInit() {
    this.dSubscribtion = this.store.select(fromDonationStore.getAllAttendeeDonations).subscribe(d => {
      this.donations = d;
      this.ref.detectChanges();
    });
    this.store.select(fromAttendeeStore.getMeansOfTransportationAsEntities).subscribe(m => {
      this.meansOfTransportation = m;
      this.ref.detectChanges();
    });
  }
  ngOnDestroy() {
    this.dSubscribtion.unsubscribe();
  }
}
