import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { TourForm, TourModel } from '../../../shared/models/tour.model';
import { TourSegmentFormModel, TourSegmentModel } from '../../../shared/models/tour_segment.model';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class TourFormService {
  private tourForm: BehaviorSubject<FormGroup>;
  tourForm$: Observable<FormGroup>;

  constructor(private fb: FormBuilder) { }

  init(tour: TourModel): void {
    const currentTourFormGroup = this.fb.group(new TourForm(tour, this.fb));
    this.tourForm = new BehaviorSubject(currentTourFormGroup);
    this.tourForm$ = this.tourForm.asObservable();
  }

  addSegment(segment?: TourSegmentModel): void {
    const tourSegment: TourSegmentModel = segment || {
      id: null,
      name: '',
      description: '',
      start: {
        id: null,
        name: '',
        latitude: null,
        longitude: null,
        date: moment().format('YYYY-MM-DD')
      },
      end: {
        id: null,
        name: '',
        latitude: null,
        longitude: null,
        date: moment().format('YYYY-MM-DD')
      }
    };
    const currentTour = this.tourForm.getValue();
    const currentSegments = currentTour.get('tourSegments') as FormArray;

    currentSegments.push(
      this.fb.group(
        new TourSegmentFormModel(tourSegment, this.fb)
      )
    );
    this.tourForm.next(currentTour);
  }

  deleteSegment(i: number): void {
    const currentTour = this.tourForm.getValue();
    const currentSegments = currentTour.get('tourSegments') as FormArray;

    currentSegments.removeAt(i);
    this.tourForm.next(currentTour);
  }

  getModelData(): TourModel {
    const currentTour = this.tourForm.getValue();
    const currentSegments = currentTour.get('tourSegments') as FormArray;
    const currentStart = currentTour.get('start').value as FormGroup;
    const currentEnd = currentTour.get('end').value as FormGroup;
    const currentOrganizer = currentTour.get('organizer').value as FormGroup;

    const tour: TourModel = {
      id: currentTour.get('id').value,
      title: currentTour.get('title').value,
      description: currentTour.get('description').value,
      type: currentTour.get('type').value.get('id').value,
      state: currentTour.get('state').value,
      tourSegments: [],
      organizer: {
        id: currentOrganizer.get('id').value,
        name: currentOrganizer.get('name').value,
        type: currentOrganizer.get('type').value
      },
      start: {
        id: currentStart.get('id').value,
        name: currentStart.get('name').value,
        latitude: currentStart.get('latitude').value,
        longitude: currentStart.get('longitude').value,
        date: currentStart.get('pointDate').value
      },
      end: {
        id: currentEnd.get('id').value,
        name: currentEnd.get('name').value,
        latitude: currentEnd.get('latitude').value,
        longitude: currentEnd.get('longitude').value,
        date: currentEnd.get('pointDate').value
      },
    };

    currentSegments.controls.forEach(segmentControl => {
      const segmentStart = segmentControl.get('start').value as FormGroup;
      const segmentEnd = segmentControl.get('end').value as FormGroup;
      const segment: TourSegmentModel = {
        id: segmentControl.get('id').value,
        name: segmentControl.get('name').value,
        description: segmentControl.get('description').value,
        start: {
          id: segmentStart.get('id').value,
          name: segmentStart.get('name').value,
          latitude: segmentStart.get('latitude').value,
          longitude: segmentStart.get('longitude').value,
          date: segmentStart.get('pointDate').value
        },
        end: {
          id: segmentEnd.get('id').value,
          name: segmentEnd.get('name').value,
          latitude: segmentEnd.get('latitude').value,
          longitude: segmentEnd.get('longitude').value,
          date: segmentEnd.get('pointDate').value
        }
      };
      tour.tourSegments.push(segment);
    });
    return tour;
  }
}
