import { Component, OnInit, Input } from '@angular/core';
import { TourModel } from 'src/app/shared/models/tour.model';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import { DonationHelperService } from 'src/app/shared/services/donation-helper.service';

@Component({
  selector: 'app-tour-list-row',
  templateUrl: './tour-list-row.component.html',
  styleUrls: ['./tour-list-row.component.scss']
})
export class TourListRowComponent implements OnInit {
  @Input() tour: TourModel = null;
  @Input() mots: {[id: string]: MeanOfTransportationModel} = {};
  constructor() { }

  ngOnInit(): void {
  }
  get color(): string {
    const by = this.tour.type as string;
    return DonationHelperService.getColor(by, this.mots);
  }
}
