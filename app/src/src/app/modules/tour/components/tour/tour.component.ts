import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TourModel } from 'src/app/shared/models/tour.model';
import * as fromTourStore from '../../../../shared/store';
import * as fromDataStore from 'src/app/shared/store';
import { FormGroup } from '@angular/forms';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.scss']
})
export class TourComponent implements OnInit {
  tours$: Observable<TourModel[]>;
  mots$: Observable<{[id: string]: MeanOfTransportationModel}>;
  formGroup: FormGroup;

  constructor(private store: Store<fromTourStore.AppState>) {}

  ngOnInit() {
    this.tours$ = this.store.pipe(select(fromTourStore.getAllTours));
    this.mots$ = this.store.pipe(select(fromDataStore.getMeansOfTransportationAsEntities));
    this.store.dispatch(new fromTourStore.LoadTours());
  }
}
