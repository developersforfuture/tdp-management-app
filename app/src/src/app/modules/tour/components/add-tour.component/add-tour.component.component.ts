import { Component, OnInit } from '@angular/core';
import { TourModel, TourState } from 'src/app/shared/models/tour.model';
import * as fromDataStore from 'src/app/shared/store';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { TourOrganizerType } from 'src/app/shared/models/tour_organizer.model';

@Component({
  selector: 'app-add-tour.component',
  templateUrl: './add-tour.component.component.html',
  styleUrls: ['./add-tour.component.component.scss']
})
export class AddTourComponent implements OnInit {
  mot$: Observable<{[id: string]: MeanOfTransportationModel}>;
  today = moment().format('d/m/y');
  tour: TourModel = {
    id: null,
    description: '',
    title: '',
    start: {
      longitude: null,
      latitude: null,
      name: '',
      date: this.today
    },
    end: {
      longitude: null,
      latitude: null,
      name: '',
      date: this.today
    },
    type: '',
    organizer: {
      name: '',
      type: TourOrganizerType.SINGLE
    },
    tourSegments: [],
    state: TourState.DRAFT,
  };
  constructor(private store: Store<fromDataStore.AppState>) { }

  ngOnInit(): void {
    this.mot$ = this.store.select(fromDataStore.getMeansOfTransportationAsEntities);
  }

}
