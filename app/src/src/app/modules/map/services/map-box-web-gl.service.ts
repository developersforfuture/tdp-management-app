import { Injectable } from '@angular/core';
import mapboxgl from 'mapbox-gl';
import { MapConfigModel } from '../map_config.model';
import { Store } from '@ngrx/store';
import * as fromMapStore from 'src/app/shared/store';
import * as fromDataStore from 'src/app/shared/store';
import { getMapDonationEntitiesAsGeoJSON } from '../../../shared/store/selectors/map_data.selectors';
import { MeanOfTransportationModel } from 'src/app/shared/models/mean_of_transportaion.model';

@Injectable({
  providedIn: 'root',
})
export class MapBoxWebGLService {
  map: any;
  meansOfTransportations: {[id: string]: MeanOfTransportationModel} = {};
  constructor(private store: Store<fromMapStore.AppState>) {

   }

  initMap(config: MapConfigModel): void {
    mapboxgl.accessToken =
      'pk.eyJ1IjoiZWxlY3RyaWNtYXh4IiwiYSI6ImNrYWJ6ZnQ2ajBnZ2wycXF5cWszaDdsbWEifQ.cA3TwpAs2hUiJXzrDMJBAQ';
    // mapboxgl.workerUrl = 'https://api.mapbox.com/mapbox-gl-js/v1.10.0/mapbox-gl-csp-worker.js';
    const center = [config.longitude, config.latitude];
    const map = new mapboxgl.Map({
      container: config.map,
      style: 'mapbox://styles/mapbox/light-v10',
      attributionControl: false,
      boxZoom: false,
      center,
      doubleClickZoom: false,
      dragPan: true,
      dragRotate: false,
      interactive: true,
      keyboard: false,
      pitchWithRotate: false,
      scrollZoom: false,
      touchZoomRotate: false,
      zoom: config.zoom,
    });
    this.store.select(fromDataStore.getMeansOfTransportationAsEntities).subscribe(mot => {
      this.meansOfTransportations = mot;
      if (Object.keys(mot).length) {
        this.addDonationSource(map);
        this.addOwnTourSource(map);
      }
    });

    map.addControl(new mapboxgl.NavigationControl());
    this.map = map;
  }

  private addDonationSource(map: any) {
    map.on('load', () => {
      const clusterProperties = {};
      const circleColors: any = ['case'];
      const colors = [];
      const motsById = {};
      for (const id in this.meansOfTransportations) {
        if (this.meansOfTransportations.hasOwnProperty(id)) {
          const mot = this.meansOfTransportations[id];
          colors.push(mot.color);
          motsById[mot.iconClass] = mot;
          const propertyCase = ['==', ['get', 'by'], id];
          clusterProperties[mot.iconClass] = ['+', ['case',  propertyCase, ['get', 'amount'], 0]];
          circleColors.push(propertyCase);
          circleColors.push(mot.color);
        }
      }
      circleColors.push('#FFF');

      map.addSource('donations', {
        type: 'geojson',
        cluster: true,
        clusterMaxZoom: 14, // Max zoom to cluster points on
        clusterRadius: 50, // Radius of each cluster when clustering points (defaults to 50)
        data: {type: 'FeatureCollection', features: []},
        clusterProperties
      });
      map.addLayer({
        id: 'donation_circle',
        type: 'circle',
        source: 'donations',
        filter: ['!=', 'cluster', true],
        paint: {
          'circle-color': circleColors,
          'circle-opacity': 0.6,
          'circle-radius': 20
        }
      });
      map.addLayer({
        id: 'donation_label',
        type: 'symbol',
        source: 'donations',
        filter: ['!=', 'cluster', true],
        layout: {
          'text-field': [
            'number-format',
            ['get', 'amount'],
            { 'min-fraction-digits': 1, 'max-fraction-digits': 1 }
          ],
          'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
          'text-size': 17
        },
        paint: {
          'text-color': '#FFF'
        }
      });

      // objects for caching and keeping track of HTML marker objects (for performance)
      const markers = {};
      let markersOnScreen = {};

      function updateMarkers() {
        const newMarkers = {};
        const features = map.querySourceFeatures('donations');

        // for every cluster on the screen, create an HTML marker for it (if we didn't yet),
        // and add it to the map if it's not there already
        for (const feature of features) {
          const coords = feature.geometry.coordinates;
          const props = feature.properties;
          if (!props.cluster) {
            continue;
          }
          const id = props.cluster_id;

          let marker = markers[id];
          if (!marker) {
            const el = createDonutChart(props);
            marker = markers[id] = new mapboxgl.Marker({
              element: el
            }).setLngLat(coords);
          }
          newMarkers[id] = marker;

          if (!markersOnScreen[id]) {
            marker.addTo(map);
          }
        }
        // for every marker we've added previously, remove those that are no longer visible
        for (const id in markersOnScreen) {
          if (markersOnScreen.hasOwnProperty(id)) {
            if (!newMarkers[id]) {
              markersOnScreen[id].remove();
            }
          }
        }
        markersOnScreen = newMarkers;

        // after the GeoJSON data is loaded, update markers on the screen and do so on every map move/moveend
        map.on('data', (e) => {
          if (e.sourceId !== 'donations' || !e.isSourceLoaded) {
            return;
          }

          map.on('move', updateMarkers);
          map.on('moveend', updateMarkers);
          updateMarkers();
        });
      }

      updateMarkers();

      // code for creating an SVG donut chart from feature properties
      function createDonutChart(props) {
        const offsets = [];
        const counts = Object.keys(clusterProperties).map(key => props.hasOwnProperty(key) ? props[key] : 0);
        let total = 0;
        for (const i of counts) {
          offsets.push(total);
          total += i;
        }
        const fontSize =
          total >= 1000 ? 22 : total >= 100 ? 20 : total >= 10 ? 18 : 16;
        const r = total >= 1000 ? 50 : total >= 100 ? 32 : total >= 10 ? 24 : 18;
        const r0 = Math.round(r * 0.6);
        const w = r * 2;

        let html =
          '<div><svg width="' + w + '" height="' + w + '" viewbox="0 0 ' + w + ' ' + w + '" text-anchor="middle" style="font: ' +
          fontSize + 'px sans-serif">';
        for (let i = 0; i < counts.length; i++) {
          html += donutSegment(
            0 !== total ? offsets[i] / total : 0,
            0 !== total ? (offsets[i] + counts[i]) / total : 0,
            r,
            r0,
            colors[i]
          );
        }
        html +=
          '<circle cx="' + r + '" cy="' + r + r0 + '" fill="white" />' +
          '<text dominant-baseline="central" transform="translate(' + r + ', ' + r + ')">' +
          total.toLocaleString() +
          '</text></svg></div>';

        const el = document.createElement('div');
        el.innerHTML = html;
        return el.firstChild;
      }

      function donutSegment(start, end, r, r0, color) {
        if (end - start === 1) {
          end -= 0.00001;
        }
        const a0 = 2 * Math.PI * (start - 0.25);
        const a1 = 2 * Math.PI * (end - 0.25);
        const x0 = Math.cos(a0);
        const y0 = Math.sin(a0);
        const x1 = Math.cos(a1);
        const y1 = Math.sin(a1);
        const largeArc = end - start > 0.5 ? 1 : 0;

        return [
          '<path d="M', r + r0 * x0, r + r0 * y0, 'L', r + r * x0, r + r * y0, 'A', r, r, 0, largeArc, 1, r + r * x1, r +
          r * y1, 'L', r + r0 * x1, r + r0 * y1, 'A', r0, r0, 0, largeArc, 0, r + r0 * x0, r + r0 * y0, '" fill="' + color + '" />'
        ].join(' ');
      }

      this.store.select(getMapDonationEntitiesAsGeoJSON).subscribe((donations) => {
        const source = map.getSource('donations');
        if (source) {
          source.setData(donations);
          return;
        }
      });
    });
  }

  private addOwnTourSource(map: any) {
    map.on('load', () => {
    });
  }
}
