export interface EditAttendeePasswordModel {
    oldPlainPassword: string;
    plainPassword: string;
}
