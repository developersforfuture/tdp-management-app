export interface EditAttendeeProfileModel {
    username: string;
    mail: string;
    firstName: string;
    secondName: string;
    organisation: number;
    country: number;
}
