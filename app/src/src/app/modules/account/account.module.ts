import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { AttendeesService } from '../../shared/services/attendees.service';
import { HttpClientModule } from '@angular/common/http';
import { EditAttendeeComponent } from './components/edit-attendee-profile/edit_attendee.component';
import { EditAttendeeProfileComponent } from './components/edit-attendee-tour-profile/edit_attendee_profile.component';
import { EditAttendeePasswordComponent } from './components/edit-password/edit_attendee_password.component';
import { AccountComponent } from './components/account/account.component';
export const accountRoutes: Routes = [
  { path: 'dashboard', component: AccountComponent },
  { path: 'profile', component: EditAttendeeComponent },
  {
    path: 'tour-profile',
    component: EditAttendeeProfileComponent
  },
  { path: 'change-password',
    component: EditAttendeePasswordComponent
  }
];

@NgModule({
  declarations: [
    AccountComponent,
    EditAttendeeComponent,
    EditAttendeePasswordComponent,
    EditAttendeeProfileComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(accountRoutes),
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [AttendeesService]
})
export class AccountModule { }
